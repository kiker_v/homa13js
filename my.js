const images = document.querySelectorAll(".image-to-show")
let timer; 
let index = 0 
const show = () => {
    const timer = setInterval(() => {
       index++
       console.log(index)
        if(index === images.length){
            index = 0
        }
       images.forEach((element, idx) => {
        if(index == idx){
              element.style.display = 'block' // apacity 1
        }else{element.style.display = 'none'} // opacity 0 css transition:all 0.6
       })
    },3000)
    return timer
}
timer = show()
const btnStop = document.querySelector('.stop')
const btnStart = document.querySelector('.start')
btnStop.addEventListener('click', () => {
    clearInterval(timer)
    btnStart.disabled = false
    btnStop.disabled = true
})
btnStart.addEventListener('click', () => {
    timer = show()
    btnStart.disabled = true
    btnStop.disabled = false
})
